#!/bin/bash

./run_tests.sh

./run_linters.sh

echo "Running: \
    sonar-scanner $* 
"
sonar-scanner $*
